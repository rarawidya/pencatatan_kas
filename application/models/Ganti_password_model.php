<?php

class Ganti_password_model extends CI_Model {

	public function __construct() 
	{
		parent::__construct();
		$this->load->database();
	}

	public function update_password($id,$password) 
	{
		$this->db->set('password', $password);
		$this->db->where('id_user', $id);
		$this->db->update('users');
	
	}
	public function cek_user($id, $password)
	{
		$result = array();

		//siapkan query builder
		$this->db->from('users');
		$this->db->where('password', $password);
		$this->db->where('id_user', $id);

		//eksekusi query
		$query = $this->db->get();

		//cek hasil query
		if ($query->num_rows() > 0) {
			$result['valid_user'] = true;
		}
		else {
			$result['valid_user'] = false;
		}

		return $result;
	}

}
