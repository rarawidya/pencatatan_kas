<?php

class Pembelian_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function ambil_semua_pembelian()
	{

		//siapkan query builder
		$this->db->from('pembelian');
		$this->db->join('lawantransaksi', 'lawantransaksi.id_lawantransaksi = pembelian.id_lawantransaksi');

		//eksekusi query
		/*$query = $this->db->get();*/

		return $this->db->get()->result();
		
	}
	public function ambil_semua_lawantransaksi()
	{
		return $this->db->get('lawantransaksi')->result();
		
	}
	public function tambah_pembelian($no_fak, $tanggal, $total, $lawantransaksi, $keterangan  )
	{
		$this->nofaktur_pembelian = $no_fak;
		$this->tgl_pembelian = $tanggal;
		$this->totalharga_pembelian = $total;
		$this->id_lawantransaksi = $lawantransaksi;
		$this->keterangan_pembelian = $keterangan;

		$this->db->insert('pembelian',$this);

		return $this->db->insert_id();
	}

	public function hapus_pembelian($id)
	{
		$this->db->delete('pembelian', array('id_pembelian' => $id));
	}
	public function ambil_pembelian($id)
	{
		return $this->db->get_where('pembelian', ['id_pembelian' => $id])->row();
		//maksudnya sama seperti = select * from pembelian where id_pembelian=$id
	}

	public function update_pembelian($id)
	{
		//ambil inputan
		$no_fak = $this->input->post('no_fak');
		$tanggal = $this->input->post('tanggal');
		$total = $this->input->post('total');
		$lawantransaksi = $this->input->post('lawantransaksi');
		$keterangan = $this->input->post('keterangan');
		

		$this->nofaktur_pembelian = $no_fak;
		$this->tgl_pembelian = $tanggal;
		$this->totalharga_pembelian = $total;
		$this->id_lawantransaksi = $lawantransaksi;
		$this->keterangan_pembelian = $keterangan;

		$this->db->update('pembelian', $this, ['id_pembelian' => $id]);
	}
	public function ambil_laporan_pembelian()
	{

		$tglAwal = $this->input->post('tglawal');
		$tglAkhir = $this->input->post('tglakhir');

		//siapkan query builder
		$this->db->from('pembelian');
		$this->db->join('lawantransaksi', 'lawantransaksi.id_lawantransaksi = pembelian.id_lawantransaksi');
		$this->db->where('tgl_pembelian >=', $tglAwal);
		$this->db->where('tgl_pembelian <=', $tglAkhir);

		return $this->db->get()->result();
		
	}
	public function ambil_semua_laporan_pembelian()
	{

		//siapkan query builder
		$this->db->from('pembelian');
		$this->db->join('lawantransaksi', 'lawantransaksi.id_lawantransaksi = pembelian.id_lawantransaksi');

		return $this->db->get()->result();
		
	}
}
