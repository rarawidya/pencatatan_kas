<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jeniskas extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('jeniskas_model');
		$this->load->library('session');

		if(!$this->session->userdata('status_login')){
		$this->session->set_flashdata('error', 'Anda harus login terlebih dahulu');
			redirect(base_url());
		}
	}
	public function index()
	{
		// ambil data kas
		$data_jeniskas = $this->jeniskas_model->ambil_semua_jeniskas(); 

		// masukkan data ke array yang akan dipassing ke view
		$data['jeniskas'] = $data_jeniskas;

		$this->load->view('header_view');
		$this->load->view('menu_view');
		$this->load->view('Content/Jenis_Kas/data_jeniskas.php', $data);
		$this->load->view('footer_view');
	}
	public function index_form()
	{
		// ambil data jeniskas
		$data_jeniskas= $this->jeniskas_model->ambil_semua_jeniskas(); 

		// masukkan data ke array yang akan dipassing ke view
		$data['jeniskas'] = $data_jeniskas;

		$this->load->view('header_view');
		$this->load->view('menu_view');
		$this->load->view('content/Jenis_Kas/tambah_jeniskas', $data);
		$this->load->view('footer_view');
	}
	public function create()
	{
		$jeniskas = $this->input->post('jeniskas');
		$keterangan_jeniskas= $this->input->post('keterangan');

	 	$this->jeniskas_model->tambah_jeniskas($jeniskas, $keterangan_jeniskas);
	
		$this->session->set_flashdata('success', 'Tambah jenis kas berhasil');

		redirect('jeniskas');
	}
	public function delete($id)
	{
		$this->jeniskas_model->hapus_jeniskas($id);

		$this->session->set_flashdata('success', "Hapus jenis kas berhasil");

		redirect('jeniskas');
	}
	public function edit($id= null)//handling error tanpa id
{ 
	//handling error tanpa id
	if(!isset($id)) redirect('jeniskas');

	$data['jeniskas'] = $this->jeniskas_model->ambil_jeniskas($id);
		$this->load->view('header_view');
		$this->load->view('menu_view');
		$this->load->view('Content/Jenis_Kas/edit_jeniskas.php',$data); //content 
		$this->load->view('footer_view');
}

	public function update($id = null)
{
	if(!isset($id)) redirect('jeniskas');

	 $this->jeniskas_model->update_jeniskas($id);
	
	$this->session->set_flashdata('success', "Edit jenis kas berhasil");
			
	redirect('jeniskas');
}
}