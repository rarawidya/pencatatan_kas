<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keluar extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('keluar_model');
		$this->load->library('session');

		if(!$this->session->userdata('status_login')){
		$this->session->set_flashdata('error', 'Anda harus login terlebih dahulu');
			redirect(base_url());
		}
	}
	public function index()
	{
		// ambil data lawantransaksi
		$data_keluar = $this->keluar_model->ambil_semua_keluar(); 

		// keluarkan data ke array yang akan dipassing ke view
		$data['keluar'] = $data_keluar;

		$this->load->view('header_view');
		$this->load->view('menu_view');
		$this->load->view('content/Transaksi_keluar/data_keluar', $data);
		$this->load->view('footer_view');
	}
	public function index_form()
	{
		// ambil data lawantransaksi
		$data_lawantransaksi= $this->keluar_model->ambil_semua_lawantransaksi(); 

		// keluarkan data ke array yang akan dipassing ke view
		$data['lawantransaksi'] = $data_lawantransaksi;

		// ambil data lawantransaksi
		$data_jeniskas= $this->keluar_model->ambil_semua_jeniskas(); 
 
		// keluarkan data ke array yang akan dipassing ke view
		$data['jeniskas'] = $data_jeniskas;

		$this->load->view('header_view');
		$this->load->view('menu_view');
		$this->load->view('content/Transaksi_keluar/tambah_keluar', $data);
		$this->load->view('footer_view');
	}
	public function create()
	{
		$tanggal = $this->input->post('tanggal');
		$lawantransaksi= $this->input->post('lawantransaksi');
		$kredit = $this->input->post('kredit');
		$jeniskas = $this->input->post('jeniskas');
		$keterangan = $this->input->post('keterangan');

	 	$this->keluar_model->tambah_keluar($tanggal, $lawantransaksi, $kredit, $jeniskas, $keterangan);
	
		$this->session->set_flashdata('success', 'Tambah Transaksi Keluar berhasil');

		redirect('keluar');
	}
	public function delete($id)
	{
		$this->keluar_model->hapus_keluar($id);

		$this->session->set_flashdata('success', "Hapus Transaksi Keluar berhasil");

		redirect('keluar');
	}
	public function edit($id= null)//handling error tanpa id
{ 
	//handling error tanpa id
	if(!isset($id)) redirect('keluar');

	$data['keluar'] = $this->keluar_model->ambil_keluar($id);
	$data['lawantransaksi'] = $this->keluar_model->ambil_semua_lawantransaksi();
	$data['jeniskas'] = $this->keluar_model->ambil_semua_jeniskas(); 

		$this->load->view('header_view');
		$this->load->view('menu_view');
		$this->load->view('Content/Transaksi_keluar/edit_keluar.php',$data); //content 
		$this->load->view('footer_view');
}

	public function update($id = null)
{
	if(!isset($id)) redirect('keluar');

	 $this->keluar_model->update_keluar($id);
	
	$this->session->set_flashdata('success', "Edit Transaksi Keluar berhasil");
			
	redirect('keluar');
}
}