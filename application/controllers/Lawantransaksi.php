<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lawantransaksi extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('lawantransaksi_model');
		$this->load->library('session');

		if(!$this->session->userdata('status_login')){
		$this->session->set_flashdata('error', 'Anda harus login terlebih dahulu');
			redirect(base_url());
		}
	}
	public function index()
	{
		// ambil data kas
		$data_lawantransaksi = $this->lawantransaksi_model->ambil_semua_lawantransaksi(); 

		// masukkan data ke array yang akan dipassing ke view
		$data['lawantransaksi'] = $data_lawantransaksi;

		$this->load->view('header_view');
		$this->load->view('menu_view');
		$this->load->view('Content/Lawantransaksi/data_lawantransaksi.php', $data);
		$this->load->view('footer_view');
	}
	public function index_form()
	{
		// ambil data lawantransaksi
		$data_lawantransaksi= $this->lawantransaksi_model->ambil_semua_lawantransaksi(); 

		// masukkan data ke array yang akan dipassing ke view
		$data['lawantransaksi'] = $data_lawantransaksi;

		$this->load->view('header_view');
		$this->load->view('menu_view');
		$this->load->view('content/Lawantransaksi/tambah_lawantransaksi', $data);
		$this->load->view('footer_view');
	}
	public function create()
	{
		$npwp = $this->input->post('npwp');
		$nama = $this->input->post('nama');
		$alamat= $this->input->post('alamat');
		$keterangan = $this->input->post('keterangan');

	 	$this->lawantransaksi_model->tambah_lawantransaksi($npwp, $nama, $alamat,$keterangan);
	
		$this->session->set_flashdata('success', 'Tambah Lawan Transaksi  berhasil');

		redirect('lawantransaksi');
	}
	public function delete($id)
	{
		$this->lawantransaksi_model->hapus_lawantransaksi($id);

		$this->session->set_flashdata('success', "Hapus Lawan Transaksi berhasil");

		redirect('lawantransaksi');
	}
	public function edit($id= null)//handling error tanpa id
{ 
	//handling error tanpa id
	if(!isset($id)) redirect('lawantransaksi');

	$data['lawantransaksi'] = $this->lawantransaksi_model->ambil_lawantransaksi($id);
		$this->load->view('header_view');
		$this->load->view('menu_view');
		$this->load->view('Content/Lawantransaksi/edit_lawantransaksi.php',$data); //content 
		$this->load->view('footer_view');
}

	public function update($id = null)
{
	if(!isset($id)) redirect('lawantransaksi');

	 $this->lawantransaksi_model->update_lawantransaksi($id);
	
	$this->session->set_flashdata('success', "Edit Lawan Transaksi berhasil");
			
	redirect('lawantransaksi');
}
}