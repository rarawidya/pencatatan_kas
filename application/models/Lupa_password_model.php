<?php
class Lupa_password_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function cek_email($email)
	{
		$result = array();

		//siapkan query builder
		$this->db->from('users');
		$this->db->where('email', $email);
		
		//eksekusi query
		return $this->db->get();
	}
	public function insert_token($token, $email){

		$this->token = $token;

		$this->db->update('users', $this, ['email' => $email]);
	}
	public function cek_token($token, $id) {
     	$result = array();

		//siapkan query builder
		$this->db->from('users');
		$this->db->where('token', $token);
		$this->db->where('id_user', $id);
		
		
		//eksekusi query
		return $this->db->get();
	}
	public function update_password($id, $password){

		$this->password = $password;

		$this->db->update('users', $this, ['id_user' => $id]);
	}
}
