<?php

class Penjualan_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function ambil_semua_penjualan()
	{

		//siapkan query builder
		$this->db->from('penjualan');
		$this->db->join('lawantransaksi', 'lawantransaksi.id_lawantransaksi = penjualan.id_lawantransaksi');

		//eksekusi query
		/*$query = $this->db->get();*/

		return $this->db->get()->result();
		
	}
	public function ambil_semua_lawantransaksi()
	{
		return $this->db->get('lawantransaksi')->result();
		
	}
	public function tambah_penjualan($no_fak, $tanggal, $total, $ppn, $dpp, $lawantransaksi, $keterangan  )
	{
		$this->nofaktur_penjualan = $no_fak;
		$this->tgl_penjualan = $tanggal;
		$this->totalharga_penjualan = $total;
		$this->ppn = $ppn;
		$this->dpp = $dpp;
		$this->id_lawantransaksi = $lawantransaksi;
		$this->keterangan_penjualan = $keterangan;

		$this->db->insert('penjualan',$this);

		return $this->db->insert_id();
	}

	public function hapus_penjualan($id)
	{
		$this->db->delete('penjualan', array('id_penjualan' => $id));
	}
	public function ambil_penjualan($id)
	{
		return $this->db->get_where('penjualan', ['id_penjualan' => $id])->row();
		//maksudnya sama seperti = select * from penjualan where id_penjualan=$id
	}

	public function update_penjualan($id)
	{
		//ambil inputan
		$no_fak = $this->input->post('no_fak');
		$tanggal = $this->input->post('tanggal');
		$total = $this->input->post('total');
		$ppn = $this->input->post('ppn');
		$dpp = $this->input->post('dpp');
		$lawantransaksi = $this->input->post('lawantransaksi');
		$keterangan = $this->input->post('keterangan');
		

		$this->nofaktur_penjualan = $no_fak;
		$this->tgl_penjualan = $tanggal;
		$this->totalharga_penjualan = $total;
		$this->ppn = $ppn;
		$this->dpp = $dpp;
		$this->id_lawantransaksi = $lawantransaksi;
		$this->keterangan_penjualan = $keterangan;

		$this->db->update('penjualan', $this, ['id_penjualan' => $id]);
	}

	public function ambil_laporan_penjualan()
	{

		$tglAwal = $this->input->post('tglawal');
		$tglAkhir = $this->input->post('tglakhir');

		//siapkan query builder
		$this->db->from('penjualan');
		$this->db->join('lawantransaksi', 'lawantransaksi.id_lawantransaksi = penjualan.id_lawantransaksi');

		$this->db->where('tgl_penjualan >=', $tglAwal);
		$this->db->where('tgl_penjualan <=', $tglAkhir);

		return $this->db->get()->result();
		
	}
	public function ambil_semua_laporan_penjualan()
	{
		$this->db->from('penjualan');
		$this->db->join('lawantransaksi', 'lawantransaksi.id_lawantransaksi = penjualan.id_lawantransaksi');

		return $this->db->get()->result();
		
	}
}