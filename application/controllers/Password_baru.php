<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Password_baru extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library(['session','form_validation']);
		$this->load->helper('url');
		$this->load->model('password_baru_model');
	}
   
     public function index()  
     {  
     	$this->load->view('passbaru_view');
         
     }
 }
