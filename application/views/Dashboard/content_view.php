<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <center><strong><marquee behavior="alternate" onmouseover="this.stop()" onmouseout="this.start()" direction="right"> SELAMAT DATANG DI SISTEM PENCATATAN KAS CV. TRI NATA TEKNIKINDO </marquee></strong></center></h1>
      
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-cart-plus"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"><strong>Pembelian</strong></span>
              <span class="info-box-number"><small>Banyak Transaksi :</small> <?php echo $pembelian;?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-shopping-cart"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"><strong>Penjualan</strong></span>
              <span class="info-box-number"><small>Banyak Transaksi :</small> <?php echo $penjualan;?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class=" fa fa-cloud-download"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"><strong>Transaksi Masuk</strong></span>
              <span class="info-box-number"><small>Banyak Transaksi :</small> <?php echo $masuk;?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-cloud-upload"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"><strong>Transaksi Keluar</strong></span>
              <span class="info-box-number"><small>Banyak Transaksi :</small> <?php echo $keluar;?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Laporan Kas Bulanan</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <p class="text-center">
                    <?php $tgl_awal = date('Y').'-01-01';?>
                    <strong>Transaksi: <?php echo date('d M Y',strtotime($tgl_awal)); ?> - <?php echo date('d M Y'); ?></strong>
                  </p>

                  <div class="chart">
                    <!-- Sales Chart Canvas -->
                    <canvas id="kasChart" style="height: 180px;"></canvas>
                  </div>
                  <!-- /.chart-responsive -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- ./box-body -->
                <?php
                    $sk = 0;
                    foreach ($keluarSaldo as $item) {
                      $sk += $item->kredit;
                      
                    }
                    $sm = 0;
                    foreach ($masukSaldo as $items) {
                      $sm += $items->debit;
                      
                    }
                    $profit = $sm-$sk;
                  ?>
            <div class="box-footer">
              <div class="row">
                <div class="col-sm-4 col-xs-6">
                  <div class="description-block border-right">
                    <h5 class="description-header"><?php echo "Rp. " . number_format($sm,2,',','.');?></h5>
                    <span class="description-text">Debit</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-4 col-xs-6">
                  <div class="description-block border-right">
                    <h5 class="description-header"><?php echo "Rp. " . number_format($sk,2,',','.');?></h5>
                    <span class="description-text">Kredit</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-4 col-xs-6">
                  <div class="description-block border-right">
                    <h5 class="description-header"><?php echo "Rp. " . number_format($profit,2,',','.');?></h5>
                    <span class="description-text">TOTAL PROFIT</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
