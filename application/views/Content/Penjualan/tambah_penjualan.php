<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <center><strong>Sistem Pencatatan Kas</strong></center>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('index.php/beranda'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Penjualan</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <div class="col-md-12">
                    <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title"><strong><font color=blue>Form Tambah Penjualan</font></strong></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="<?php echo base_url('index.php/penjualan/create'); ?>" method="post" enctype="multipart/form-data">

              <div class="box-body">
                <div class="form-group">
                  <label for="no_fak" class="col-sm-2 control-label">No. Faktur Penjualan</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="no_fak" name="no_fak" placeholder="Nomor Faktur Penjualan" required>
                  </div>
                </div>
                 <div class="form-group">
                  <label for="no_fak" class="col-sm-2 control-label">Tanggal</label>
                  <div class="col-sm-10"> 
                  <input type="date" class="form-control" id="tanggal" name="tanggal" placeholder="Tanggal">
                  </div> 
                </div>
                <div class="form-group">
                  <label for="total" class="col-sm-2 control-label">Harga</label>
                  <div class="col-sm-10">
                    <input type="number" class="form-control" id="total" name="total" placeholder="Total Harga" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="ppn" class="col-sm-2 control-label">PPN</label>
                  <div class="col-sm-10">
                    <input type="number" class="form-control" id="ppn" name="ppn" onkeyup="hitung();" placeholder="PPN" >
                  </div>
                </div>
                <div class="form-group">
                  <label for="dpp" class="col-sm-2 control-label">DPP</label>
                  <div class="col-sm-10">
                    <input type="number" class="form-control" id="dpp" name="dpp" placeholder="DPP" readonly>
                  </div>
                </div>
                <div class="form-group">
                  <label for="lawantrans" class="col-sm-2 control-label">NPWP Lawan Transaksi</label>
                  <div class="col-sm-10">
                    <select class="form-control" id="lawantransaksi" name="lawantransaksi">
                      <option value="">-- Silahkan Pilih Lawan Transaksi --</option>
                       <?php foreach($lawantransaksi as $items){ ?>
                        <option value="<?php echo $items->id_lawantransaksi; ?>"><?php echo $items->npwp. " - ". $items->nama_lawantransaksi; ?></option>
                        <?php } ?>
                    </select>
                  </div>
                </div>
                 <div class="form-group">
                  <label for="keterangan" class="col-sm-2 control-label">Keterangan</label>
                  <div class="col-sm-10">
                    <textarea id="keterangan" name="keterangan" rows="10" cols="80" placeholder="Keterangan"></textarea>
                  </div>
                </div>
                
  
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="<?php echo base_url('index.php/penjualan');?>" class="btn btn-default"><i class="fa fa-times-circle"></i> Cancel</a>
                <button type="submit" class="btn btn-info pull-right"><i class="fa fa-save"></i> Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
