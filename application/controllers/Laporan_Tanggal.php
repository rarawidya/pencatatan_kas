<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_Tanggal extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('laporan_tanggal_model');
		$this->load->library('session');

		if(!$this->session->userdata('status_login')){
		$this->session->set_flashdata('error', 'Anda harus login terlebih dahulu');
			redirect(base_url());
		}
	}
	public function index_lap()
	{

		$tglAwal = $this->input->post('tglawal');
		$tglAkhir = $this->input->post('tglakhir');

		if (isset($tglAwal, $tglAkhir)){
			$data_laporan_tanggal = $this->laporan_tanggal_model->ambil_semua_laporan_tanggal($tglAwal, $tglAkhir);
		}
		else {
			$data_laporan_tanggal = $this->laporan_tanggal_model->ambil_semua_laporan(); 
		}

		// masukkan data ke array yang akan dipassing ke view
		$data['laporan_tanggal'] = $data_laporan_tanggal;
		
		//return $data['laporan_tanggal'];
		$this->load->view('content/Laporan/Laporan_Tanggal', $data);
	}
	public function index()
	{
		$this->load->view('header_view');
		$this->load->view('menu_view');
		$this->load->view('Content/Laporan/Filter_tanggal_kas.php');
		$this->load->view('footer_view');
	}
}