<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
         <center><strong>Sistem Pencatatan Kas</strong></center>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('index.php/beranda'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Pembelian</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <div class="col-md-12">
                    <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Form Edit Pembelian</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="<?php echo base_url('index.php/pembelian/update/'.$pembelian->id_pembelian); ?>" method="post">

              <div class="box-body">
                <div class="form-group">
                  <label for="no_fak" class="col-sm-2 control-label">No. Faktur pembelian</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="no_fak" name="no_fak" placeholder="Nomor Faktur pembelian" value="<?php echo $pembelian->nofaktur_pembelian; ?>" required>
                  </div>
                </div>
                 <div class="form-group">
                  <label for="tanggal" class="col-sm-2 control-label">Tanggal</label>
                  <div class="col-sm-10"> 
                  <input type="date" class="form-control" id="tanggal" name="tanggal" placeholder="Tanggal" value="<?php echo $pembelian->tgl_pembelian; ?>">
                  </div> 
                </div>
                <div class="form-group">
                  <label for="total" class="col-sm-2 control-label">Harga</label>
                  <div class="col-sm-10">
                    <input type="number" class="form-control" id="total" onkeyup="hitung();" name="total" placeholder="Total Harga" required value="<?php echo $pembelian->totalharga_pembelian; ?>">
                  </div>
                </div>
                
                <div class="form-group">
                  <label for="lawantrans" class="col-sm-2 control-label">NPWP Lawan Transaksi</label>
                  <div class="col-sm-10">
                    <select class="form-control" id="lawantransaksi" name="lawantransaksi">
                       <?php foreach($lawantransaksi as $items){ ?>
                        <?php if ($items->id_lawantransaksi == $pembelian->id_lawantransaksi){?>
                       <option value="<?php echo $items->id_lawantransaksi; ?>" selected>
                          <?php echo $items->npwp. " - ". $items->nama_lawantransaksi; ?></option> 
                      <?php }
                      else {?>
                        <option value="<?php echo $items->id_lawantransaksi; ?>">
                          <?php echo $items->npwp. " - ". $items->nama_lawantransaksi; ?></option>
                    
                      
                        <?php }} ?>
                    </select>
                  </div>
                </div>
                 <div class="form-group">
                  <label for="keterangan" class="col-sm-2 control-label">Keterangan</label>
                  <div class="col-sm-10">
                    <textarea id="keterangan" name="keterangan" rows="10" cols="80" placeholder="Keterangan"> <?php echo $pembelian->keterangan_pembelian; ?> </textarea>
                  </div>
                </div>
                
  
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href= "<?php echo base_url('index.php/pembelian');?>" class="btn btn-default"><i class="fa fa-times-circle"></i> Cancel</a>
                <button type="submit" class="btn btn-info pull-right"><i class="fa fa-save"></i> Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
