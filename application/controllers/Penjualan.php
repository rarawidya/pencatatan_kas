<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('penjualan_model');
		$this->load->library('session');

		if(!$this->session->userdata('status_login')){
		$this->session->set_flashdata('error', 'Anda harus login terlebih dahulu');
			redirect(base_url());
		}
	}
	public function index()
	{
		// ambil data lawantransaksi
		$data_penjualan = $this->penjualan_model->ambil_semua_penjualan(); 

		// masukkan data ke array yang akan dipassing ke view
		$data['penjualan'] = $data_penjualan;

		$this->load->view('header_view');
		$this->load->view('menu_view');
		$this->load->view('content/Penjualan/data_penjualan', $data);
		$this->load->view('footer_view');
	}
	public function index_form()
	{
		// ambil data lawantransaksi
		$data_lawantransaksi= $this->penjualan_model->ambil_semua_lawantransaksi(); 

		// masukkan data ke array yang akan dipassing ke view
		$data['lawantransaksi'] = $data_lawantransaksi;

		$this->load->view('header_view');
		$this->load->view('menu_view');
		$this->load->view('content/Penjualan/tambah_penjualan', $data);
		$this->load->view('footer_view');
	}
	public function index_lap()
	{
		$data_penjualan = $this->penjualan_model->ambil_laporan_penjualan(); 

		// masukkan data ke array yang akan dipassing ke view
		$data['penjualan'] = $data_penjualan;

		$this->load->view('content/Laporan/Laporan_Penjualan', $data);
	}
	public function index_lap_tgl()
	{
		$this->load->view('header_view');
		$this->load->view('menu_view');
		$this->load->view('Content/Laporan/Filter_tanggal_penjualan.php');
		$this->load->view('footer_view');
	}
	public function index_semua_lap()
	{
		$data_penjualan = $this->penjualan_model->ambil_semua_laporan_penjualan(); 

		// masukkan data ke array yang akan dipassing ke view
		$data['penjualan'] = $data_penjualan;

		$this->load->view('content/Laporan/Laporan_Penjualan', $data);
	}
	public function create()
	{
		$no_fak = $this->input->post('no_fak');
		$tanggal = $this->input->post('tanggal');
		$total= $this->input->post('total');
		$ppn = $this->input->post('ppn');
		$dpp = $this->input->post('dpp');
		$lawantransaksi = $this->input->post('lawantransaksi');
		$keterangan = $this->input->post('keterangan');

	 	$this->penjualan_model->tambah_penjualan($no_fak, $tanggal, $total, $ppn, $dpp, $lawantransaksi, $keterangan);
	
		$this->session->set_flashdata('success', 'Tambah Penjualan berhasil');

		redirect('penjualan');
	}
	public function delete($id)
	{
		$this->penjualan_model->hapus_penjualan($id);

		$this->session->set_flashdata('success', "Hapus penjualan berhasil");

		redirect('penjualan');
	}
	public function edit($id= null)//handling error tanpa id
{ 
	//handling error tanpa id
	if(!isset($id)) redirect('penjualan');

	$data['penjualan'] = $this->penjualan_model->ambil_penjualan($id);
	$data['lawantransaksi'] = $this->penjualan_model->ambil_semua_lawantransaksi();
		$this->load->view('header_view');
		$this->load->view('menu_view');
		$this->load->view('Content/Penjualan/edit_penjualan.php',$data); //content 
		$this->load->view('footer_view');
}

	public function update($id = null)
{
	if(!isset($id)) redirect('penjualan');

	 $this->penjualan_model->update_penjualan($id);
	
	$this->session->set_flashdata('success', "Edit penjualan berhasil");
			
	redirect('penjualan');
}
}
