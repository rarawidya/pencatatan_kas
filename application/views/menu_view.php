<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">

    <!-- Logo -->
    <a href="<?php echo base_url('index.php/beranda'); ?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>TNT</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>CV.TRI NATA TEKNIKINDO</b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url('assets/img/tnt.jpeg'); ?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $this->session->userdata('nama'); ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo base_url('assets/img/tnt.jpeg'); ?>" class="img-circle" alt="User Image">

                <p>
                  <?php echo $this->session->userdata('nama'); ?>

                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo base_url('index.php/ganti_password') ?>" class="btn btn-warning btn-flat"><i class="fa fa-key" aria-hidden="true"></i> Ganti Password</a>
                </div>
                <div class="pull-right">
                  <a class="btn btn-danger btn-flat" data-toggle="modal" data-target="#confirmModal"><i class="fa fa-sign-out" aria-hidden="true"></i> Sign out</a>
                </div>
              </li>
            </ul>
          </li>            
        </ul>
      </div>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-database"></i>
            <span>Master</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>

          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('index.php/lawantransaksi'); ?>"><i class="fa fa-circle-o"></i>Lawan Transaksi</a></li>
            <li><a href="<?php echo base_url('index.php/jeniskas'); ?>"><i class="fa fa-circle-o"></i> Jenis Kas</a></li>
            </ul>
          </li>

        <li>
          <a href="<?php echo base_url('index.php/penjualan'); ?>">
            <i class="fa fa-shopping-cart"></i> <span>Penjualan</span>
            
          </a>
        </li>

        <li>
          <a href="<?php echo base_url('index.php/pembelian'); ?>">
            <i class="fa fa-cart-plus"></i> <span>Pembelian</span>
            
          </a>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i>
            <span>Pencatatan Transaksi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>

          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('index.php/masuk'); ?>"><i class="fa fa-cloud-download"></i> Transaksi Masuk</a></li>
            <li><a href="<?php echo base_url('index.php/keluar'); ?>"><i class="fa fa-cloud-upload"></i> Transaksi Keluar</a></li>
          </ul>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Laporan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>

          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('index.php/laporan_tanggal/index');?>"><i class="fa fa-circle-o"></i> Transaksi Kas</a></li>
            <li><a href="<?php echo base_url('index.php/penjualan/index_lap_tgl');?>"><i class="fa fa-circle-o"></i> Rekap Penjualan</a></li>
            <li><a href="<?php echo base_url('index.php/pembelian/index_lap_tgl');?>"><i class="fa fa-circle-o"></i> Rekap Pembelian</a></li>
          </ul>
        </li>
       
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>